__<!> This is a WIP documentation, It may change, evolve and contains errors or
dead links<!>__

# Chained-ci

Chained-ci is a solution to chain CI/CD pipelines from different gilab projects.
It leverages gitlab APIs and features like the artifacts.

![Chained-ci overview](img/chained-CI.png)

As an example if you want to install a solution on an infrastructure, you can
chain different projects:
  - infrastructure deployment
  - deployment of the target solution
  - test the target solution

## Chained-CI Philosophy

Chained-CI goal is to __glue together__ several git projects to complete a
 __scenario__.
Each step of a pipeline is managed by a standalone project, letting the
project team decide how to run the different steps the project needs, using
gitlab-ci. Chained-CI will __only__ be responsible of passing __config files__
from one project to the other.

## What Chained-CI do or do not

Chained-CI is:
  - a __scenario manager__

It can:
  - Trigger compatible gitlab projects with:
    - Setting environment variables
    - An archive containing files mixing different sources:
      - Static files from a repository
      - Dynamic files from a previous step
      - Dynamic files from a previous scenario
  - Prepare environment files for SSH connection to any target server. This
    implies that chained-ci can run a project from a gitlab.com runner in
    a private infrastructure.
  - Help deploying the same project on different target
  - Recover artifacts from a step (aka: a project pipeline)
  - Cipher artifacts to avoid security issues

It can not:
  - Generate config file from a template. It can only serve static files from
    a local or remote project or ask for a project to generate it.
  - use project that are not prepared for chained-ci (see chained-ci-tools
    project)

## Get started

* [Definitions](./user-guide/definitions.md)
* [Create your Chained-ci](./user-guide/configure.md)
* [Run your Chained-ci](./user-guide/run.md)
* [Examples](./example/example.md)
* [Hands on training](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-handson/blob/master/README.md)

## How To

* [Manage artifacts](./howto/manage_artifacts.md)
* [Use a private infrastructure](./howto/use_private_infra.md)
* [Add a project to chained-ci](./howto/add_project.md)
* [Advanced - Change the gitlab-ci structure](./howto/changegitstructure.md)

## File description

* [pod_inventory/inventory](./user-guide/inventory.md)
* [pod_inventory/group_vars/all.yml](./user-guide/all.md)
* [pod_inventory/host_vars/my_scenario.yml](./user-guide/my_scenario.md)
* [Files input for scenarios](./user-guide/scenario_config.md)

## Chained-ci project

Chained-ci is composed of several sub-projects:

* [Chained-ci](https://gitlab.forge.orange-labs.fr/osons/chained-ci): the part
  where the chains will be created and configured
* [Chained-ci-roles](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-roles):
  the repository hosting the ansible roles
* [Chained-ci-tools](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-tools):
  some tooling to simplify the creation of the chains
* [Chained-ci-vue](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-vue):
  a web front end to easily visualise the different chains.

![chained-ci-vue](img/chained-ci-ui.png)

If you click on any element of the chain, you will see the corresponding gitlab
pipeline. You can also restart the chain in case of failure at the step it
failed.

## Why Chained-ci

Historically involved in several Open Source projects, it was very hard to
harmonize CI/CD strategies as each project had its own way to manage it and
automation culture.
OPNFV XCI initiative was created to provide a way to create links between CI/CD
teams of different Open Source Networking communities.

In parallel, internally we started having lots of projects on gitlab.
The goal was to be able to chain the pipelines of these different gitlab
projects. Each project remains responsible for its CI/CD strategy but may expose
artifacts and can be consumed by other projects.

Thanks to Chained-ci, it is possible to easily mix and match pipelines and unify
CI/CD strategy leveraging gitlab capabilities.

It is possible on the same infrastructure to automate the deployments and
testing of several projects in parallel.
