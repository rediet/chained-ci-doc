# Examples of a Chained-ci

A complete example is available on [chained-ci-examples](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-examples)

The Chained-ci has been declared in the [pod_inventory/inventory](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-examples/blob/master/pod_inventory/inventory).

![Chained-ci example1](../img/example1.png)

The description of this chain can be found under the
pod_inventory/host_vars directory

![Chained-ci example2](../img/example2.png)

## basic chain: example1

The content of example1.yml is described as follows:

```YAML
---
jumphost:
  server: localhost
  user:
scenario_steps:
  config:
    project: config
    infra: NONE
    get_encrypt: false
    get_bin: true
    get_artifacts:
      - name: example1
        static_src: true
  step1:
    project: dummy1
    get_artifacts: config
    get_encrypt: false
    get_bin: true
    pull_artifacts: "job2"
  step2:
    project: dummy2
    get_artifacts: step1
    get_bin: true
    get_encrypt: false
    pull_artifacts: "job2"
```

This basic chain calls the project [chained-ci-dummy1](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy1)
then the project
[chained-ci-dummy2](https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-dummy2)
from the jumhost localhost.

Artifacts are exchanged between the projects using the git variables
`get_artifacts` and `pull_artifacts`. The first artifact downloaded for dummy1
project is a static artifact defined in the `config` step.  
The artifacts are stored in binary format. That is specified by
`get_bin: true`.  
`get_encrypt: false` indicates that the artifact is not encrypted.  

All the other gitlab variables and references are indicated in the ansible var
file [pod_inventory/group_vars/all.yml](https://gitlab.com/Orange-OpenSource/
lfn/ci_cd/chained-ci-examples/blob/master/pod_inventory/group_vars/all.yml)

It includes:

* project name
* project git url
* project API url
* timeout value
* additional extra parameters

## advanced chain: example2

The chain can be described as follows:

![Chained-ci example3](../img/example3.png)

This chain is representative of the chaining of the different pipelines
(Operating System installation > OpenStack Installation > OPNFV Functest
OpenStack check) done in Orange OpenLab. It could be complexified with the
addition of other chaines such as > Kubernetes installation > OPNFV Functest
Kubernetes check > ONAP installation > ONAP check.

This chain is not implemented in chained-ci-examples and can be defined in
pod_inventory/host_vars as follows:

```YAML
---
jumphost:
  server: srv10.infra.opnfv.fr
  user: debian
environment: baremetal/pod1
scenario_steps:
  config:
    project: config
    certificates: vim.pod1.opnfv.fr.eyml
  # As we split infra_manager we must set extra TAG and get/pull artifacts
  infra_install:
    project: infra_manager
    pull_artifacts: "install:triggered"
    extra_parameters:
      TAG: install
  infra_deploy:
    project: infra_manager
    get_artifacts: infra_install
    extra_parameters:
      TAG: deploy
  # deploy kolla as VIM
  vim_deploy:
    project: kolla
  # healthcheck with functest
  vim_test:
    project: functest
    get_artifacts: vim_deploy
  trigger:
    project: trigger
```

The chain deals with 5 steps:

* config: from static files, we generate the initial configuration files
* infra_install: this step deals with bare metal server configuration
* infra_deploy: this step ensures the hardware installation is completed
* vim_deploy: this step deals with the automatic installation of OpenStack
* vim: this step deals with functional check of the installed OpenStack using
  OPNFV functest test suites

4 gitlab projects are invoked here:

* config
* infra_manager (called 2 times)
* kolla
* functest

Please note that these projects are mentioned to illustrate the chained-ci.
They are hosted in an internal gitlab CE infrastructure and not yet available
on gitlab.com. You can obviously use your own projects.

The infra_deploy step asks for the artifacts from the infra_manager.

The vim_test step asks for the vim_deploy artifact.
