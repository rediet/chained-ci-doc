# How to manage artifacts

Artifacts may be managed in `all.yml` and/or in the chained-id definition under
`pod_inventory/host_vars`.

## Main options

The following variables are proposed:

* get_artifacts
* pull_artifacts
* artifacts
* get_bin
* get_encrypt

### get_artifacts

The get_artifacts variable allows to retrieve data (env variables,
configuration files) from a previous gitlab pipeline from a chained-ci.

These artifacts can be retrieved:

* dynamically (by Default): gitlab API will be called to retrieve the last
  artifacts corresponding to the referenced project
* statically: if static_src parameter is set to True, you then must indicate
  the path to the artifact

The path to the all static artifacts is defined in file `all.yml`under
pod_inventory/group_vars. A `config` project is defined in the git_projects
section.

```YAML
git_projects:
    config:
      stage: config
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
      branch: "{{ lookup('env','config_branch')|default('master', true) }}"
      path: repo_config
```

The static artifacts are stored under the directory
repo_config/config/artifacts in this example.

#### limit_to

Please note that it is possible to retrieve only a subset of the files from
an artifact by using **limit_to**.

```YAML
get_artifacts:
      - name: infra_deploy
        limit_to:
          - vars/user_cloud.yml: vars/user_cloud.yml
```

#### in_pipeline=false

It is also possible to indicate that the artifact cannot be retrieved from the
pipeline execution but from a specific step by setting in_pipeline=false in the
get_artifact section. You then have to add the name of the pipeline to fetch
and the step(s) in the pipeline.

In the example, we retrieve artifacts from `config` step of last
`onap_k8s_ic_pod4` pipeline.

```YAML
 get_artifacts:
      - name: config:onap_k8s_ic_pod4
        in_pipeline: false
        limit_to:
          - vars/pdf.yml: vars/pdf.yml
          - vars/idf.yml: vars/idf.yml
```

### pull_artifacts

pull_artifacts indicates the job's name that compiles the artifact object whereas get_artifacts specifies the project name that produced the artifact.

In the example below, this is job1-dummy1 of project chained-ci-dummy1 that builds the artifact. It is then used by the project dummy-test.

```yaml
---
jumphost:
scenario_steps:
  project1:
    project: chained-ci-dummy1
    branch: ex2<name>
    pull_artifacts: "job1-dummy1"
  test-project1:
    project: dummy-test
    branch: ex2<name>
    get_artifacts: project1
```

### artifacts

The job handling the artifact shall specify the files to add or to retrieve
from the artifact object.

In this first example, only the vars directory of the artifact object is used by the
job.

```yaml
job1-dummy2:
  <<: *chained_ci_tools
  <<: *runner
  stage: stage1
  artifacts:
    paths:
      - vars
  script:
    - echo "this is a first job (job1) of project dummy2"
    - echo "Artifact from dummy1 project is now reachable"
    - ls -al ./vars
```

In this second example, the artifact variable is used to build the artifact
content.

```yaml
job1-dummy1:
  <<: *chained_ci_tools
  stage: stage1
  script:
    - echo "this is a first job in stage 1"
    - echo "Artifact generation"
    - mkdir ${PWD}/vars; mkdir ${PWD}/inventory
    - date > ${PWD}/vars/date
  artifacts:
    paths:
      - vars
      - inventory
  <<: *runner_tags
  only:
    refs:
      - triggers
```

### get_bin

This get_bin variable set to true leads to build the artifact in binary format.

### get_encrypt

This get_encrypt variable set to true permits to encryp artifact content based
on a predefined ANSIBLE_VAULT_PASSWORD variable.

## Specific static artifact

As previously mentionned, the path to all static artifact is defined in file
`all.yml` under pod_inventory/group_vars.

By default, the static artifact shall contain specific list of files. This is
defined by infra variable. Without specific infrastructure definition,
`infra:NONE` shall be set.

### infra

This option specifies that you want to use specific resources of an
existing infra. This infra parameter refers an Installer Descriptor File (IDF) and an Platform Descriptor File (PDF).
There are yml files describing the infrastructure. It is stored like
static artifacts under the config path: repo_config/config according to config
project definition in the following example:

```YAML
git_projects:
    config:
      stage: config
      url: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci
      branch: "{{ lookup('env','config_branch')|default('master', true) }}"
      path: repo_config
```

The PDF file is named \<chained-ci name\>.yml.  
The IDF file is named idf-\<chained-ci name\>.yml

### Static artifact creation

Static artifact consists in a zip file containing all the files.  

For instance if you need to precise an inventory my_jumhost and a set of
variables declared in a foo.yml file, you will need to prepare your file.

```md
├── inventory
│   └── my_jumphost
└── vars
    └── foo.yml
```

Zip the different files in my_foo.zip and reference it in the chained-ci.

```YAML
infra: NONE
get_artifacts:
  - name: my_foo
    static_src: true
```

As a reminder, all the static artifacts are stored according to the path
parameter value defined within the config project in section git_projects.  

`infra:NONE` is set when PDF or IDF files are not required.

## Basic inventory file creation

At artifact initialization, the file `inventory/inventory` is created using jumphost configuration parameters.

```YAML
jumphost ansible_host={{ jumphost.server }}
      ansible_user={{ jumphost.user }} pod={{ Chained_ci_name }}
```

As a reminder jumphost parameters are defined in chained-ci chain definition file.

```YAML
---
jumphost:
  server: remote_vm
  user: debian
```
